# function to convert integers upto 9999 to roman equivalent
# input is taken through standard input


def roman(n):
    print n
    s = str(n)
    l = len(s)
    romanstr = ""
    lst = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
    while l > 0:
        #print s
        #print romanstr
        if int(s) == 0:
            return romanstr
        elif l > 3:
            n1 = int(s[0])
            romanstr += "M"*n1
            l = l-1
            s = s[1:]
        elif l > 2:
            if int(s) >= 500:
                n1 = int(s[0])
                romanstr += "D"
                n1 = n1-5
                romanstr += "C"*n1
                l = l-1
                s = s[1:]
            elif int(s) >= 400 and int(s) < 500:
                romanstr += "CD"
                l = l-1
                s = s[1:]
                
            else:
                n1 = int(s[0])
                romanstr += "C"*n1
                l = l-1
                s = s[1:]
        
        elif l > 1:
            if int(s) >= 50 and int(s) < 90:
                n1 = int(s[0])
                romanstr += "L"
                n1 = n1-5
                romanstr += "X"*n1
                l = l-1
                s = s[1:]
            
            elif int(s) >= 40 and int(s) < 50:
                romanstr += "XL"
                l = l-1
                s = s[1:]
                
            elif int(s) >= 90 and int(s) < 100:
                romanstr += "XC"
                l = l-1
                s = s[1:]
                
            else:
                n1 = int(s[0])
                romanstr += "X"*n1
                l = l-1
                s = s[1:]
        elif l == 1:
            n1 = int(s[0])
            romanstr += lst[n1-1]
            l = l-1
    return romanstr
print roman(int(raw_input()))
