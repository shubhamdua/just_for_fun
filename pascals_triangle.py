def pascal(n):
    lst1 = []
    for i in range(n):
        lst1.append(list())
    x = 1
    lc = -1
    for lst in lst1:
        lc += 1
        for i in range(x):
            if i == 0 or i == x-1:
                lst.append(1)
            else:
                m = lst1[lc-1][i-1] + lst1[lc-1][i]
                lst.append(m)
        x += 1
    space = 0
    for i in lst1[::-1]:
        str1 = " " * space
        for j in i:
            str1 += str(j) + " "
        print str1
        space += 1
pascal(20)