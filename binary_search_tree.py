class Stack:
    def __init__(self):
        self.stack = []
    def push(self, value):
        self.stack.append(value)
    def pop(self):
        if len(self.stack) > 0:
            n = self.stack[-1]
            del self.stack[-1]
            return n
        else:
            return None
    def isEmpty(self):
        if len(self.stack) > 0:
            return False
        else:
            return True
    def peek(self):
        if self.isEmpty == False:
            return self.stack[-1]
        else:
            return None
class Queue:
    def __init__(self):
        self.q = []
    def enqueue(self, data):
        if data != None:
            self.q.append(data)
        else:
            pass
    def dequeue(self):
        if self.q[0] != None:
            n = self.q[0]
            del self.q[0]
            return n
        else:
            return None
    def isEmpty(self):
        if len(self.q) > 0:
            return False
        else:
            return True
class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None
        
class BinaryTree:
    def __init__(self):
        self.root = None
    def add(self, value):
        node = Node(value)
        if self.root == None:
            self.root = node
            return
        else:
            n = self.root
            while True:
                if n.value < node.value:
                    if n.right == None:
                        n.right = node
                        return
                    else:
                        n = n.right
                elif n.value >= node.value:
                    if n.left == None:
                        n.left = node
                        return
                    else:
                        n = n.left
                
    def bfs(self):
        q = Queue()
        lst = []
        q.enqueue(self.root)
        while q.isEmpty() == False:
            node = q.dequeue()
            q.enqueue(node.left)
            q.enqueue(node.right)
            lst.append(node.value)
        print lst
    def preorder(self, root):
        print root.value
        if root.left != None:
            self.preorder(root.left)
        if root.right != None:
            self.preorder(root.right)
    def postorder(self, root):
        if root != None:
            self.postorder(root.left)
            self.postorder(root.right)
            print root.value
    def inorder(self, root):
        if root != None:
            self.inorder(root.left)
            print root.value
            self.inorder(root.right)
bt = BinaryTree()
bt.add(4)
bt.add(2)
bt.add(6)
bt.add(1)
bt.add(3)
bt.add(5)
bt.add(7)
bt.bfs()
bt.preorder(bt.root)
bt.inorder(bt.root)
bt.postorder(bt.root)